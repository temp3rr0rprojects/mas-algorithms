from __future__ import print_function
import copy,sys, pprint

class State():
    def __init__(self,name):
        self.__name__ = name

class Goal():
    def __init__(self,name):
        self.__name__ = name

def forall(seq,cond):
    for x in seq:
        if not cond(x): return False
    return True

def find_if(cond,seq):
    for x in seq:
        if cond(x): return x
    return None

operators = {}
methods = {}

def declare_operators(*op_list):
    operators.update({op.__name__:op for op in op_list})
    return operators

def declare_methods(task_name,*method_list):
    methods.update({task_name:list(method_list)})
    return methods[task_name]

def pyhop(state,tasks,verbose=0):
    return seek_plan(state,tasks,[],0,verbose)

def seek_plan(state,tasks,plan,depth,verbose=0):
    if tasks == []:
        return plan
    task1 = tasks[0]
    if task1[0] in operators:
        operator = operators[task1[0]]
        newstate = operator(copy.deepcopy(state),*task1[1:])
        if newstate:
            solution = seek_plan(newstate,tasks[1:],plan+[task1],depth+1,verbose)
            if solution != False:
                return solution
    if task1[0] in methods:
        relevant = methods[task1[0]]
        for method in relevant:
            subtasks = method(state,*task1[1:])
            if subtasks != False:
                solution = seek_plan(state,subtasks+tasks[1:],plan,depth+1,verbose)
                if solution != False:
                    return solution
    return False